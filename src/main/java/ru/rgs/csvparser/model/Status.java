package ru.rgs.csvparser.model;

public enum Status {
    COMPLETED, FAILED, NOT_FOUND
}

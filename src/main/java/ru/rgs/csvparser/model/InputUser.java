package ru.rgs.csvparser.model;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class InputUser {

    @CsvBindByName(column = "FIRST_NAME")
    private String firstName;
    @CsvBindByName(column = "LAST_NAME")
    private String lastName;
    @CsvBindByName(column = "MIDDLE_NAME")
    private String middleName;
    @CsvBindByName(column = "CONTRACT_DATE")
    private String contractDate;
}

package ru.rgs.csvparser.model;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {

    private String clientName;
    private String contractDate;

    public UserDto(InputUser inputUser) {
        this.clientName = Stream.of(inputUser.getFirstName(), inputUser.getMiddleName(), inputUser.getLastName())
                .map(String::toUpperCase)
                .collect(Collectors.joining(" "));
        contractDate = inputUser.getContractDate();
    }
}

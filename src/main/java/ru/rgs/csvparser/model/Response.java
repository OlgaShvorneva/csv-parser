package ru.rgs.csvparser.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import ru.rgs.csvparser.exception.UndefinedStatusException;

@Data
@Slf4j
public class Response {

    public static final String NOT_FOUND_RESPONSE = "не найден";
    private Status status;
    private Double scoringValue;
    private String description;

    public String getScoring() throws UndefinedStatusException {
        switch (status) {
            case COMPLETED:
                return String.valueOf(scoringValue);
            case FAILED:
                return description.toLowerCase();
            case NOT_FOUND:
                return NOT_FOUND_RESPONSE;
            default:
                log.error("Неизвестный статус ответа от ScoringService: " + status);
                throw new UndefinedStatusException();
        }
    }
}

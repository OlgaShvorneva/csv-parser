package ru.rgs.csvparser.model;

import com.opencsv.bean.CsvBindByName;

public class OutputUser {

    @CsvBindByName(column = "CLIENT_NAME")
    private String clientName;
    @CsvBindByName(column = "CONTRACT_DATE")
    private String contractDate;
    @CsvBindByName(column = "SCORING")
    private String scoring;

    public OutputUser(UserDto userDto, String scoring) {
        this.clientName = userDto.getClientName();
        this.contractDate = userDto.getContractDate();
        this.scoring = scoring;
    }
}

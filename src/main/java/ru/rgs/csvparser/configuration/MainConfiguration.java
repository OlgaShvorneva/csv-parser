package ru.rgs.csvparser.configuration;

import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rgs.csvparser.feign.ScoringService;
import ru.rgs.csvparser.feign.ScoringErrorDecoder;
import ru.rgs.csvparser.service.CsvParserService;
import ru.rgs.csvparser.service.impl.CsvParserServiceImpl;
import ru.rgs.csvparser.service.impl.UserScoringFiller;

@Configuration
public class MainConfiguration {

    @Bean
    public CsvParserService csvParserService() {
        return new CsvParserServiceImpl(userScoringFiller());
    }

    @Value("${feign.scoringService.url}")
    String serviceUrl;

    @Bean
    ScoringService scoringService() {
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .errorDecoder(new ScoringErrorDecoder())
                .logger(new Logger.ErrorLogger())
                .logLevel(Logger.Level.FULL)
                .target(ScoringService.class, serviceUrl);
    }

    @Bean
    UserScoringFiller userScoringFiller() {
        return new UserScoringFiller(scoringService());
    }
}

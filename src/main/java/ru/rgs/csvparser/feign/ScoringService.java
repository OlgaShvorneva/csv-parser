package ru.rgs.csvparser.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.rgs.csvparser.model.Response;
import ru.rgs.csvparser.model.UserDto;

@FeignClient(name = "scoreService")
public interface ScoringService {

    @RequestMapping(value = "/score", method = RequestMethod.POST, consumes = "application/json")
    Response getScore(UserDto user);
}

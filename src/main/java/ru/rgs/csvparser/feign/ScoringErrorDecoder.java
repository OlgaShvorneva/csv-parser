package ru.rgs.csvparser.feign;

import static feign.FeignException.errorStatus;

import feign.Response;
import feign.codec.ErrorDecoder;
import ru.rgs.csvparser.exception.ResponseStatus500Exception;

public class ScoringErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {

        if (response.status() == 500) {
            return new ResponseStatus500Exception("Scoring service request processing error",
                    response.body().toString());
        }
        return errorStatus(methodKey, response);
    }

}

package ru.rgs.csvparser.exception;

public class ResponseProcessingException extends Exception {

    public ResponseProcessingException(Throwable cause) {
        super(cause);
    }

}

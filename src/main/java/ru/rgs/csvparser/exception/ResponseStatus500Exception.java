package ru.rgs.csvparser.exception;

import feign.FeignException;
/**
 * When the status is 500, it stores the response body,
 */
public class ResponseStatus500Exception extends FeignException {
    private String responseBody;

    public ResponseStatus500Exception(String message, String responseBody) {
        super(message);
        this.responseBody = responseBody;
    }

    public String getResponseBody() {
        return responseBody;
    }
}

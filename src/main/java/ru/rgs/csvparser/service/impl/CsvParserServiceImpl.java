package ru.rgs.csvparser.service.impl;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.opencsv.CSVWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rgs.csvparser.exception.ResponseProcessingException;
import ru.rgs.csvparser.model.InputUser;
import ru.rgs.csvparser.model.UserDto;
import ru.rgs.csvparser.model.OutputUser;
import ru.rgs.csvparser.service.CsvParserService;

@Slf4j
@Service
public class CsvParserServiceImpl implements CsvParserService {

    @Value("${app.outputFilePrefix}")
    private String outputFilePrefix;
    private final UserScoringFiller userScoringFiller;

    public CsvParserServiceImpl(UserScoringFiller userScoringFiller) {
        this.userScoringFiller = userScoringFiller;
    }

    @Override
    public Path processCsv(Path source) {
        log.info("Начало обработки файла " + source + " ...");
        final List<UserDto> users = readUsers(source)
                .stream()
                .map(UserDto::new)
                .collect(Collectors.toList());

        log.info("Получение данных от Scoring Service ...");
        final List<OutputUser> fillingUsers = users.stream()
                .parallel()
                .map(userDto -> {
                    try {
                        return userScoringFiller.fillUser(userDto);
                    } catch (ResponseProcessingException e) {
                        log.error(
                                "Ошибка обработки записи " + userDto.getClientName() + " из файла "
                                        + source);
                    }
                    return new OutputUser(userDto, "");
                })
                .collect(Collectors.toList());

        Path outputPath = Paths.get(source.getParent() + outputFilePrefix + source.getFileName());
        writeOutput(fillingUsers, outputPath);
        log.info("Окончена обработка файла " + source);
        return outputPath;
    }

    private List<InputUser> readUsers(Path source) {
        log.info("Чтение данных из файла " + source);
        try (BufferedReader bufferedReader = Files
                .newBufferedReader(source, StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<InputUser> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(InputUser.class);

            CsvToBean<InputUser> csvToBean = new CsvToBeanBuilder<InputUser>(bufferedReader)
                    .withType(InputUser.class)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            return csvToBean.parse();
        } catch (IOException e) {
            log.error("Ошибка при получении данных из файла: " + source);
        }
        return new ArrayList<>();
    }

    private void writeOutput(List<OutputUser> users, Path outputPath) {
        log.info("Запись данных в файл " + outputPath);
        try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<OutputUser> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(OutputUser.class);

            StatefulBeanToCsv<OutputUser> beanToCsv =
                    new StatefulBeanToCsvBuilder<OutputUser>(writer)
                            .withMappingStrategy(strategy)
                            .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                            .build();

            beanToCsv.write(users);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException | IOException ex) {
            log.error("Ошибка при записи данных в файл: " + outputPath);
        }
    }

}

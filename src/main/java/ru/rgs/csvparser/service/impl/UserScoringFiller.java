package ru.rgs.csvparser.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import ru.rgs.csvparser.exception.ResponseStatus500Exception;
import ru.rgs.csvparser.exception.ResponseProcessingException;
import ru.rgs.csvparser.exception.UndefinedStatusException;
import ru.rgs.csvparser.feign.ScoringService;
import ru.rgs.csvparser.model.Response;
import ru.rgs.csvparser.model.UserDto;
import ru.rgs.csvparser.model.OutputUser;

@Slf4j
public class UserScoringFiller {

    private final ScoringService scoringService;

    public UserScoringFiller(ScoringService scoringService) {
        this.scoringService = scoringService;
    }

    public OutputUser fillUser(UserDto userDto) throws ResponseProcessingException {
        String scoring = getScoringString(userDto);
        return new OutputUser(userDto, scoring);
    }

    private String getScoringString(UserDto userDto) throws ResponseProcessingException {
        final Response response = getResponseFromScoringService(userDto);

        try {
            return response.getScoring();
        } catch (UndefinedStatusException e) {
            throw new ResponseProcessingException(e);
        }
    }

    private Response getResponseFromScoringService(UserDto userDto)
            throws ResponseProcessingException {
        Response response;
        try {
            response = scoringService.getScore(userDto);
        } catch (ResponseStatus500Exception e) {
            response = getResponse500(e);
        } catch (FeignException exception) {
            throw new ResponseProcessingException(exception);
        }
        return response;
    }

    private Response getResponse500(ResponseStatus500Exception e)
            throws ResponseProcessingException {
        final String responseBody = e.getResponseBody();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(responseBody, Response.class);
        } catch (IOException ex) {
            throw new ResponseProcessingException(ex);
        }
    }
}
